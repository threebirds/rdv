#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import io
import os
import sys
from PIL import Image

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

def detect_text(path=None, raw_img=None):
    """Detects text in the file.

    For static captchas true result is (true: 28, false 52, ratio 0.35)"""

    client = vision.ImageAnnotatorClient()

    # Loads the image into memory
    # with io.open(path, 'rb') as image_file:
    #    content = image_file.read()
    # for google vision it is recommended img size to be more than 1024 x 768
    if raw_img is not None:
        content = raw_img #.thumbnail([2100, 600])
    elif path is not None:
        img = Image.open(path)
        img.thumbnail([2100, 600])
        imgByteArr = io.BytesIO()
        img.save(imgByteArr, format='PNG')
        content = imgByteArr.getvalue()
    else:
        print('[ERROR] No captcha path or raw_img defined')

    response = client.annotate_image({
    "image": {
        "content": content
        },
    'features': [{
        'type': vision.enums.Feature.Type.DOCUMENT_TEXT_DETECTION
        }],
    "image_context": {
        "language_hints": ["en"]
        }
    })

    document = response.full_text_annotation
    text = document.text.replace(' ', '').strip().upper()
    return text

if __name__ == '__main__':
    #static_captchas = os.listdir("captchas")
    #captcha = static_captchas[0]

    # The name of the image file to annotate
    file_name = os.path.join(os.path.dirname(__file__), 'captchas', '3JCSL.jpg')

    debug_captcha = True
    if debug_captcha == True:
        img = Image.open(file_name)
        img.show()

        text = detect_text(file_name)
        print(text)
