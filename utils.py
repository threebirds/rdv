import os
import platform
import random
from time import sleep

SYSTEM_TYPE = platform.system()
if SYSTEM_TYPE == 'Windows':
    import winsound


BEEP_FREQUENCY = 2000
BEEP_DURATION = 50
SILENT_MODE = False
LOG_LVL = 5
DEFAULT_PAUSE = (3,7)


def beep(frequency=BEEP_FREQUENCY, duration=BEEP_DURATION, silent_mode=SILENT_MODE):
    if silent_mode:
        pass
    if SYSTEM_TYPE == 'Linux':
        os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration / 1000, frequency))
    elif SYSTEM_TYPE == 'Windows':
        winsound.Beep(frequency, duration)
    else:
        log('[BEEP ERROR] OS not recognized')


# random pause
def pause(t_min=DEFAULT_PAUSE[0], t_max=DEFAULT_PAUSE[1]):
    sleep(random.triangular(t_min, t_max))


def log(msg, lvl=0, flush=False):
    if lvl <= LOG_LVL:
        print(msg, end=('' if flush else '\n'), flush=flush)
    else:
        pass
