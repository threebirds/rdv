#Telegram bot
import requests
import time


TOKEN = '609505624:AAEGPv1CL9bAJ9pN4fLLz3eBu3OPXFrY5n8'
chat_id = '208646911'
bot_URL = 'https://api.telegram.org/bot{}/'.format(TOKEN)


class Telegram_Bot():
    def __init__(self):
        pass

    def get_updates_json(self):
        response = requests.get(bot_URL + 'getUpdates')
        return response.json()

    def last_update(self, data):
        results = data['result']
        total_updates = len(results) - 1
        return results[total_updates]

    def send_mess(self, text):
        params = {'chat_id': chat_id, 'text': text}
        response = requests.post(bot_URL + 'sendMessage', data=params)
        return response

    def send_photo(self, img_URL):
        params = {'chat_id': chat_id, 'photo': img_URL}
        response = requests.post(bot_URL + 'sendPhoto', data=params)
        return response

    def wait_for_updates(self):
        initial_updates = requests.get(bot_URL + 'getUpdates').json()['result']
        current_updates = {}
        time.sleep(1)
        while True:
            current_updates = requests.get(bot_URL + 'getUpdates').json()['result']
            time.sleep(1)
            if len(current_updates) > len(initial_updates):
                return

    #last_update(get_updates_json(bot_URL))