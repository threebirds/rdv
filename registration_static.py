# TODO to add registration and analytic acts
# TODO add message if successfully registered
# TODO add logs
# TODO add script for debug
# TODO add silent mode with no beep to be a worker argument
# TODO automatic email confirmation

from io import BytesIO
import os
from time import sleep, strftime, gmtime
import random
import platform
import warnings
import requests
from PIL import Image
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as soup

from utils import beep, log, pause
import telegram_bot as tb

# captcha recognition module based on Google Vision
#from captcha_google_vision import detect_text as vision_recognize
from captcha.solving_captchas.solve_captchas_with_model import recognize_captcha as captcha_cnn

warnings.filterwarnings("ignore", "Your application has authenticated using end user credentials")


PAUSE_TIME_MIN = 2.0
PAUSE_TIME_MAX = 5.0

# TODO How to change default pause parameters values
#pause = pause(PAUSE_TIME_MIN, PAUSE_TIME_MAX)


class Registration:
    """Class for static pages that checks for free time for RDV and register user if found."""
    
    def __init__(self, user):
        self.url = user['URL']
        self.date_from = user['date_from']
        self.data = user['links']  # all requests data with personal data already inside
        self.bot = tb.Telegram_Bot()
        self.ses = None
        log('registration_static')

    def new_session(self):
        log('N:', flush=True)
        pause()
        pause()
        pause()
        self.ses = requests.Session()
        #self.ses.proxies = {}
        #self.ses.proxies['http'] = 'socks5h://127.0.0.1:9050'
        #self.ses.proxies['https'] = 'socks5h://127.0.0.1:9050'
        #self.ses.headers = {'User-Agent': UserAgent(verify_ssl=False).random, 'Accept-Language': 'fr-FR'}
        # TODO define list of actual UserAgents
        self.ses.headers = {'User-Agent': UserAgent().random}

    def print_input_fields(self, res):
        soup_inputs = soup(res.content, "lxml")
        labels = soup_inputs.find(id="ezbooking-user-form").fieldset.find_all('label')
        inputts = soup_inputs.find(id="ezbooking-user-form").fieldset.find_all('input')
        print('Labels: {}'.format(', '.join([label.get_text() for label in labels])))
        print('Tag names: {}'.format(', '.join([inputt['name'] for inputt in inputts if inputt.has_attr('name')])))

    def save_page(self, res):
        os.makedirs('saved_pages', exist_ok=True)
        f_name = '{}_page{}.html'.format(strftime("%Y_%d_%b_%H_%M_%S", gmtime()), str(res.url)[-1])
        f_name = os.path.join('saved_pages', f_name)
        with open(f_name, 'w', encoding='utf-8') as f:
            f.write(res.text)

    '''
    def check_track_mode(slot_data, mode):
        if mode == 'track':
            slot_list = slot_data.split('\n')
            os.makedirs('analytics', exist_ok=True)
            f_name = r'analytics\file_{}_page{}.html'.format('_'.join([now[4],now[1],now[2]] + now[3].split(':')), str(res.url)[-1])
            with open(f_name, 'a', encoding='utf-8') as f:
                now = time.ctime().split()
                f.write(' '.join([now[4],now[1],now[2],now[3]]) + \
                        '\t' + ' '.join([slot_list[4] + slot_list[6]]) + '\n')
            return 1
        elif mode == 'reg':
            pass
        else:
            print('Mode not recognized:', mode)
    '''

    def check_cycle(self):
        """ Main check function. Follows links until ERROR response recieved.

        return:
        """

        def check_lang(self):
            """Returns lang of a page in gormat 'en' or 'fr'. If error or language not recognized returns Null."""
            try:
                # print(self.url)
                res = self.ses.get(self.url)
                res.raise_for_status()
            except requests.HTTPError as e:
                if e.response.status_code == 403:
                    log('Forbidden_403_', flush=True)
                    return None
                elif e.response.status_code == 502:
                    log('502:', flush=True)
                    return None
            except requests.ConnectionError:
                print("No internet connection.")
                input()
                return None
            except Exception as ex:
                print('Check language error {} '.format(str(ex)))
                pause()
                return None
            mysoup = soup(res.content, "lxml")

            phrase_raw = mysoup.find(id='condition_Booking')
            print('type(phrase_raw):', type(phrase_raw))
            if phrase_raw is not None:
                phrase = phrase_raw.label.text
            if phrase == "Please accept conditions to continue booking process.":
                return 'en'
            elif phrase == "Veuillez cocher la case pour accepter les conditions d'utilisation avant de continuer le " \
                           "processus de prise de rendez-vous.":
                return 'fr'
            else:
                print('Language not detected:' + phrase)
                return None

        def check_captcha(page_no):
            if page_no == '6':
                return True

        def print_free_time(self, res):
            slot_soup = soup(res.content, "lxml")
            slot_data = slot_soup.find('fieldset').text
            print('\n'.join(slot_data.split('\n')[2:7:2]), '\n')

        def download_captcha(self, save='captcha/raw_captchas_static/', show=False):
            res = self.ses.get('http://www.hauts-de-seine.gouv.fr/ezjscore/call/bookingserver::captcha')
            captcha_soup = soup(res.content, "lxml")
            img_URL = captcha_soup.find('img')['src']
            response = self.ses.get(img_URL)
            image = Image.open(BytesIO(response.content))
            if save:
                os.makedirs(save, exist_ok=True)
                f_name = os.path.join(save, 'pic_%s.jpg' % strftime("%Y_%d_%b_%H_%M_%S", gmtime()))
                image.save(f_name)
            if show:
                image.show()
            return image

        def recognize_captcha_cnn(self, lang):
            img = download_captcha(self)
            captcha_text = captcha_cnn(img)
            self.data[lang]['6']['eZHumanCAPTCHACode'] = captcha_text

        '''
        def recognize_captcha_vision(self, lang):
            """With use of Google Vision."""
            img = download_captcha(self)
            captcha_text = vision_recognize(img)
            self.data[lang]['6']['eZHumanCAPTCHACode'] = captcha_text
        '''

        def check_last_page(self, page_no):
            if page_no == 9:
                self.bot.send_mess('Successfully registered! Check email!')
                input('Successfully registered! Press any button to continue')
                return True

        # ========== MAIN SCRIPT ==========

        self.new_session()
        lang = check_lang(self)
        if lang is None:
            return None
        url = self.url

        # ------------- Cycling between first '0', '2' pages -------------

        # if returned 'cycling error': should start new session
        # else: free time found, continues registration
        while url[-1] in ['0', '1', '2']:
            pause()
            try:
                res = self.ses.post(url, data=self.data[lang][url[-1]])
                res.raise_for_status()
            except requests.HTTPError as e:
                if e.response.status_code == 403:
                    log('Forbidden_403:', flush=True)
                    return None
                elif e.response.status_code == 502:
                    log('502:', flush=True)
                    return None
            url = res.url
            log(url[-1], flush=True)
        if url[-1] == '3':
            log('\nTIME LOADED')
            beep()
            self.bot.send_mess('time loaded')
        else:
            return None

        prev_url = '3'
        while url[-1] in ['3', '4', '6', '8']:
            log(url[-1])
            try:
                res = self.ses.post(url, data=self.data[lang][url[-1]])
                # if the result page is not changed then some data is missed or is wrong format
                if prev_url == res.url[-1]:
                    log(f"Can't go to next page. Error in requests data on page {prev_url}")
                    self.print_input_fields(res)
                    return 'Error in requests data'
                if res.url[-1] == '9':
                    log("Successfully registered! Check email for appointment confirmation.")
                    beep(frequency=2200)
                    pause()
                    beep(frequency=2420)
                    return 'done!'
                prev_url = res.url[-1]
                url = res.url
            except Exception as e:
                # TODO check for different error types and log error code
                print('Cycling error', e, end=' ')
                return None

            if check_captcha(res.url[-1]):
                recognize_captcha_cnn(self, lang)

            print(url[-1], end='', flush=True)

    # TODO to write register and analytics conditions
    # TODO write code to automatically begin registration of the next client

    def start_registration(self, register=True, analytics=True):
        #print('%s\n%s, %s\n%s' %(self.url, self.data['en']['8']['firstname'], self.data['en']['8']['lastname'],
        #                         self.data['en']['8']['email']))
        # print('\nregister: %s\nanalytics: %s' %(register, analytics))
        status = None
        while not status:
            status = self.check_cycle()
        print('Client {}, {} successfully registered. Check {} to confirm'.format(self.data['en']['8']['firstname'],
                                                                                  self.data['en']['8']['lastname'],
                                                                                  self.data['en']['8']['email']))
        return


if __name__ == '__main__':
    pass
    # user_reg = Registration()
    # user_reg.start_registration()
