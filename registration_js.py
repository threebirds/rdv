"""
Module for book RDV in prefecture Nanterre based on Selenium web driver

Le système de prise de rendez-vous du service des étrangers de la préfecture de Nanterre.
URL = https://rdv-etrangers-92.interieur.gouv.fr/eAppointmentpref92/element/jsp/specific/pref92.jsp
"""

import numpy as np
import os
import base64
from time import strftime, gmtime
from PIL import Image
import warnings
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, UnexpectedAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
#from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

import pytesseract
# captcha recognition module that uses Google Vision
# from captcha_google_vision import detect_text as vision_recognize

from utils import beep, log, pause
import telegram_bot as tb

warnings.filterwarnings("ignore", "Your application has authenticated using end user credentials")
os.environ["TESSDATA_PREFIX"] = r"C:\Program Files (x86)\Tesseract-OCR\tessdata"
pytesseract.pytesseract.tesseract_cmd = r"C:/Program Files (x86)/Tesseract-OCR/tesseract.exe"
#print(os.environ.get('TESSDATA_PREFIX'))

#Attributes available for "By" class:
#ID = "id"
#XPATH = "xpath"
#LINK_TEXT = "link text"
#PARTIAL_LINK_TEXT = "partial link text"
#NAME = "name"
#TAG_NAME = "tag name"
#CLASS_NAME = "class name"
#CSS_SELECTOR = "css selector"


URL_JS = 'https://rdv-etrangers-92.interieur.gouv.fr/eAppointmentpref92/element/jsp/specific/pref92.jsp'
HEADLESS_BROWSER = True
WFIND_TIMEOUT = 60


class RegistrationJS:
    """Class for dynamic (JavaScript) pages that checks for free time for RDV and register user if found."""

    def __init__(self, user):
        self.date_from = user['date_from']
        self.service_code = user["service_code"]  # integer value corresponding to service chosen on second page
        self.data = user['data']  # personal user data
        self.driver = None
        self.bot = tb.Telegram_Bot()
        log('registration_js')

    def new_session(self):
        pause()
        pause()
        options = Options()
        # To show browser or not
        options.headless = HEADLESS_BROWSER
        self.driver = webdriver.Firefox(options=options)

    def get_captcha(self):
        log('getting captcha')
        os.makedirs('js_captchas', exist_ok=True)
        f_name = os.path.join('js_captchas', 'pic_%s.jpg' %strftime("%Y_%d_%b_%H_%M_%S", gmtime()))
        img_res = self.driver.find_element_by_id("captchaImg")
        log('captcha img loaded')
        img_base64 = self.driver.execute_async_script("""
            var ele = arguments[0], callback = arguments[1];
            ele.addEventListener('load', function fn(){
              ele.removeEventListener('load', fn, false);
              var cnv = document.createElement('canvas');
              cnv.width = 300; cnv.height = 100;
              cnv.getContext('2d').drawImage(this, 0, 0);
              callback(cnv.toDataURL('image/jpeg').substring(22));
            }, false);
            ele.dispatchEvent(new Event('load'));
            """, img_res)
        with open(f_name, 'wb') as f:
            f.write(base64.b64decode(img_base64))
        return f_name

    # manual captcha recognition
    def recognize_captcha_input(self, f_name):
        with Image.open(f_name) as img:
            img.show()
        captcha_text = input('Input captcha code: ')
        os.rename(f_name, 'js_captchas/' + captcha_text + '.jpg')
        return captcha_text
        
    # pytesseract captcha recognition
    # 56% suscess on test data
    def recognize_captcha_auto(self, captcha_filename, w=134, q=2):
        log('recognizing captcha auto')

        def img_to_bw(img):
            """Image to black and white"""
            for x in range(img.size[1]):
                for y in range(img.size[0]):
                    pix = np.array(img.getpixel((y,x)))
                    if all((i > w) for i in pix):
                        img.putpixel((y,x), (255, 255, 255))
                    else:
                        img.putpixel((y,x), (0, 0, 0))
            # img.show()
            return img

        img = Image.open(r'js_captchas\%s' %  captcha_filename)
        img = img_to_bw(img)
        captcha_text = pytesseract.image_to_string(img, config='--psm 8 --oem %s -c tessedit_char_whitelist=0123456789' % q)
        log("recognized captcha {}: {}".format(captcha_filename, captcha_text))
        return captcha_text

    def recognize_captcha_google(self, img_name):
        img = Image.open(r'js_captchas\%s' % img_name)
        captcha_text = vision_recognize(raw_img=img.tobytes())
        #img.show()
        log(captcha_text)
        return captcha_text

    def wfind(self, attribute, element, timeout=WFIND_TIMEOUT):
        """ Helpful function that waits element to load on a page. """
        try:
            pause(1,1.5)
            log(getattr(By, attribute) + '...', flush=True)
            element_present = EC.presence_of_element_located((getattr(By, attribute), element))
            res = WebDriverWait(self.driver, timeout).until(element_present)
            log(f'\nRES: {res.text}')
            return res
        except UnexpectedAlertPresentException as e:
            pause(1.5,2)
            log(f'Alert present: {e}', flush=True)
            log("Alert accepted")
            log("2", flush=True)
            return 'alert'
        except TimeoutException:
            log("Timed out, recycling..")
            self.driver.close()
            self.check_cycle()

    def check_cycle(self):
        """One full cycle with accepting alerts, until successfully registered, Server Error or Permission Denied"""

        def cycling(self):
            """Function that repeats checking time accepting alerts ignoring timeouts, until free time loaded.

            If page loading timeout exceeded, returns 'timeout'
            If page successfully loaded, returns 'loaded'
            """

            res = self.wfind('XPATH', "//*[contains(text(), '3/4 : Lieu, date et heure')]")
            if res == 'alert':
                log(res)
                pause(5,7)
                self.wfind('ID', "nextButtonId").click()
                return 'alert'
            if res == 'timeout':
                log(res)
                return 'timeout'
            else:
                pause(t_min=2, t_max=3)

        def pass_captcha(self):
            """Recursevely sends read capthca text untill text accepted"""

            log('loading captcha')
            # loading captcha
            f_name = self.get_captcha()
            # recognizing captcha
            captcha_text = self.recognize_captcha_auto(f_name, w=134, q=2)
            # sending captcha text
            self.wfind("ID", "imgcId").send_keys(captcha_text)
            log(captcha_text)
            # clicking nex button
            self.wfind('ID', "nextButtonId").click()
            pause(2,4)

            # check to be no captcha error
            if self.driver.find_elements_by_xpath(
                    "//div[@class='error' and contains(text(),'Code erroné. Merci de resaisir le code de vérification.']"):
                log('captcha error')
                pass_captcha(self)

        # ========== MAIN SCRIPT ==========

        self.new_session()

        # ------------- initial page -------------
        log('_0_', flush=True)
        try:
            self.driver.get(URL_JS)
        #except requests.ConnectionError:
        #    print("No internet connection.")
        #    input()
        #    return None
        except Exception as e:
            log(e)
            self.check_cycle()

        # ---------- postcode input page ----------
        log('_1_', flush=True)
        self.wfind("ID", "CPId").send_keys(self.data["postcode"])
        self.wfind("CLASS_NAME", "btn-primary-container").click()

        # ---------- choose service page ----------
        log('_2_', flush=True)
        # wait until services loaded and check service checkbox
        self.wfind('XPATH', "//input[@name='selectedMotiveKeyList' and @value='{}']".format(self.service_code)).click()
        self.wfind('ID', "nextButtonId").click()

        # cycling ignoring (accepting) alerts until 'timeout' or 'loaded' status
        res = cycling(self)
        while res in ['timeout', 'alert']:
            self.check_cycle()

        # if free time loaded, begins registration
        assert res == 'loaded'
        print('\nTIME LOADED')
        beep()
        self.bot.send_mess('time loaded')

        # ----------------------------------------
        # ========== REGISTRATION BLOCK ==========
        # ----------------------------------------

        log('3')
        # trying to find a free time text block and print it
        log('looking for free time text block')
        try:
            buttons = self.driver.find_elements_by_xpath("//td[@class=' undefined']")
            for button in buttons:
                # print(button.get_attribute('outerHTML'))
                # print(button.get_attribute('innerHTML'))
                # looks for the first free day element
                log('day: {}'.format(button.find_element_by_tag_name("a").get_attribute('innerHTML')))
                log('month: {}'.format(button.get_attribute('data-month')))
                log('year: {}'.format(button.get_attribute('data-year')))
        except Exception as e:
            log('No free time block found')
            log(f'Reg exception: {e}')
            pause()
            self.check_cycle()
        pause(1, 1)

        log('3')
        # click on first free day
        log('clicking on first day slot')
        self.wfind("XPATH", "//td[@class=' undefined']").click()
        pause(1.5,3)

        # ---------- choose time slot page ----------

        log('5')
        # click on first free hour slot
        self.wfind("ID", "hourValueSelect")
        pause(1,2)
        hours = self.driver.find_elements_by_tag_name('option')
        for hour in hours[0:]:
            log('Free hour: {}'.format(hour.get_attribute('value')))

        log('6')
        # click on first time slot in the selected hour
        log("Clicking on the 'hours[4]' slot...")
        hours[4].click()
        self.wfind('NAME', "selectedTimeAreaBeanKey").click()
        # click 'continue'
        self.wfind('ID', "nextButtonId").click()
        pause(3,5)

        # ---------- personal info and captcha page ----------

        log('7')
        # client data input
        # civility
        self.wfind("XPATH", "//input[@type='radio'][@name='userCiv'][@value='%s']" % self.data['civility']).click()
        # last name
        self.wfind("ID", "userLastNameId").send_keys(self.data['lastname'])
        # first name
        self.wfind("ID", "userFirstNameId").send_keys(self.data['firstname'])
        # e-mail
        self.wfind("ID", "userEmailId").send_keys(self.data['email'])
        # phone number
        self.wfind("ID", "userPhoneId").send_keys(self.data['phone'])

        # CAPTCHA BLOCK

        log('8')
        pass_captcha(self)
        log('Congratulations! JS client {}, {} successfully registered. Check {} to confirm appointment.'.format(
             self.data['firstname'], self.data['lastname'], self.data['email']))
        self.bot.send_mess('Successfully registered! Check email!')


    def start_registration(self, pause_time=7):

        self.check_cycle()

            #try:
            #status = self.check_cycle(pause_time)
        '''
            except Exception as e:
                print('start_registration', e)
                pass
            finally:
                print('Closing js session..', end='\t')
                self.driver.close()
                print("Session closed")
        '''
        return 'success'


if __name__ == '__main__':
    user = {
        'service': 'js_biometrie',
        'civility': 'MR', # value 'MR'(monsieur) or 'MME'(madame)
        'firstname': 'AA',
        'lastname': 'BB',
        'email': 'eee@eee.com',
        'postcode': 92000,
        'phone': '123123123'
    }

    # 'Biométrie - prise d'empreintes digitales par suite d'une convocation préalable de la préfecture': 10
    # Monsieur/Madame Nom Prenom Email Telephone
    reg = RegistrationJS(user)
    reg.start_registration()
